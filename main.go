package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)

type User struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
}

var db *sql.DB

func main() {
	var err error
	db, err = sql.Open("mysql", "root:PnhsjskO_1941@tcp(localhost:3306)/usersdata")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/login", loginHandler)
	http.HandleFunc("/register", registerHandler)

	log.Fatal(http.ListenAndServe(":8000", nil))
}

func registerHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = createUser(user.Username, user.Password)
	if err != nil {
		http.Error(w, "Error creating user", http.StatusNotFound)
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "User created successfully")
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = findUser(user.Username, user.Password)
	if err != nil {
		http.Error(w, "User not found", http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Successful")
}

func createUser(username, password string) error {
	query := "INSERT INTO users (username,password) VALUES (?,?)"
	_, err := db.Exec(query, username, password)
	if err != nil {
		return err
	}
	return nil
}

func findUser(username, password string) error {
	var user User
	query := "SELECT id,username,password FROM users WHERE username=? AND password=?"
	err := db.QueryRow(query, username, password).Scan(&user.Id, &user.Username, &user.Password)
	if err != nil {
		if err == sql.ErrNoRows {
			return fmt.Errorf("User not found")
		}
		return err
	}
	return nil
}
